import express, { Express } from "express";
import cors from "cors";
import notFoundMiddleware from "./middlewares/not-found.middleware";
import { loggerMiddleware } from "./middlewares/logger.middleware";
import exceptionsFilter from "./middlewares/exceptions-filter";
import { html } from "./templates/index-template";
import { HttpStatusCode } from "./enums/https-statuses";

const app: Express = express();

console.log(`Index template string contains ${html.length} characters.`);

app.use(cors());
app.use(express.json());
app.use(loggerMiddleware);

app.get("/home", (req, res) => {
   res.status(HttpStatusCode.MOVED_PERMANENTLY).redirect("/");
});

app.get("/", (req, res) => {
   res.send(html);
});

app.use(notFoundMiddleware);
app.use(exceptionsFilter);

export default app;