import { Request, Response, NextFunction } from "express";
import { HttpStatusCode } from "../enums/https-statuses";
const notFoundMiddleware = (req: Request, res: Response, next: NextFunction) => {
    res.status(HttpStatusCode.NOT_FOUND).send({message: "Bad url"});
    next();
};

export default notFoundMiddleware;